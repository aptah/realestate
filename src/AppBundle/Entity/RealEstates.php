<?php
namespace AppBundle\Entity;

use A2lix\I18nDoctrineBundle\Doctrine\ORM\Util\Translatable;
use AppBundle\Entity\Embedded\GeoPoint;
use AppBundle\Entity\Traits\ActiveableTrait;
use AppBundle\Entity\Traits\BlameableTrait;
use AppBundle\Entity\Traits\TimestampableTrait;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Validator\Constraints as Assert;


/**
 * RealEstates
 *
 * @ORM\Table(name="real_estates")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\RealEstatesRepository")
 */
class RealEstates
{
    use Translatable;
    use TimestampableTrait;
    use ActiveableTrait;

    /**
     * @var GeoPoint
     *
     * @ORM\Embedded(class="AppBundle\Entity\Embedded\GeoPoint", columnPrefix=false)
     */
    private $geoPoint;
    
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     * @Assert\NotBlank()
     * @Assert\Length(min=1, max=254)
     * @ORM\Column(name="size", type="integer", nullable=true)
     */
    private $size;  

    /**
     * @var integer
     *
     * @ORM\Column(name="price", type="integer", nullable=true)
     */
    private $price;  

     /**
     * @var boolean
     *
     * @ORM\Column(name="is_for_sale", type="boolean", nullable=true)
     */
    private $isForSale;
    
     /**
     * @var integer
     *
     * @ORM\Column(name="viewed", type="integer", nullable=true)
     */
    private $viewed;  

   /**
     * @var \AppBundle\Entity\Gallery
     *
     * @ORM\ManyToOne(targetEntity="\AppBundle\Entity\Gallery", inversedBy="real_estates", cascade={"persist"})
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="gallery_id", referencedColumnName="id")
     * })
     */
    private $gallery;
    

    /**
     * @Assert\Valid
     */
    protected $translations;

    public function __construct()
    {
        $this->translations = new ArrayCollection();
        $this->geoPoint = new \AppBundle\Entity\Embedded\GeoPoint();
    }

    
    public function setTranslatableLocale($locale)
    {
        $this->locale = $locale;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set size
     *
     * @param integer $size
     *
     * @return RealEstates
     */
    public function setSize($size)
    {
        $this->size = $size;

        return $this;
    }

    /**
     * Get size
     *
     * @return integer
     */
    public function getSize()
    {
        return $this->size;
    }

    /**
     * Set price
     *
     * @param integer $price
     *
     * @return RealEstates
     */
    public function setPrice($price)
    {
        $this->price = $price;

        return $this;
    }

    /**
     * Get price
     *
     * @return integer
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * Set isForSale
     *
     * @param boolean $isForSale
     *
     * @return RealEstates
     */
    public function setIsForSale($isForSale)
    {
        $this->isForSale = $isForSale;

        return $this;
    }

    /**
     * Get isForSale
     *
     * @return boolean
     */
    public function getIsForSale()
    {
        return $this->isForSale;
    }

    /**
     * Set viewed
     *
     * @param integer $viewed
     *
     * @return RealEstates
     */
    public function setViewed($viewed)
    {
        $this->viewed = $viewed;

        return $this;
    }

    /**
     * Get viewed
     *
     * @return integer
     */
    public function getViewed()
    {
        return $this->viewed;
    }

    /**
     * Get active
     *
     * @return boolean
     */
    public function getActive()
    {
        return $this->active;
    }

    /**
     * Set geoPoint
     *
     * @param \AppBundle\Entity\Embedded\GeoPoint $geoPoint
     *
     * @return RealEstates
     */
    public function setGeoPoint(\AppBundle\Entity\Embedded\GeoPoint $geoPoint)
    {
        $this->geoPoint = $geoPoint;

        return $this;
    }

    /**
     * Get geoPoint
     *
     * @return \AppBundle\Entity\Embedded\GeoPoint
     */
    public function getGeoPoint()
    {
        return $this->geoPoint;
    }

    /**
     * Set gallery
     *
     * @param \AppBundle\Entity\Gallery $gallery
     *
     * @return RealEstates
     */
    public function setGallery(\AppBundle\Entity\Gallery $gallery = null)
    {
        $this->gallery = $gallery;

        return $this;
    }

    /**
     * Get gallery
     *
     * @return \AppBundle\Entity\Gallery
     */
    public function getGallery()
    {
        return $this->gallery;
    }
}
