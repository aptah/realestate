<?php
namespace AppBundle\Entity\Embedded;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Embeddable
 */
class GeoPoint {
    /**
     * @Assert\Type("numeric")
     * @ORM\Column(type="float",nullable=true)
     */
    public $longitude;
    
    /**
     * @Assert\Type("numeric")
     * @ORM\Column(type="float",nullable=true)
     */
    public $latitude;
    
    public function __toString()
    {
        return $this->longitude && $this->latitude ? $this->latitude.','.$this->longitude: '';
    }
    
    public function __construct($longitude = NULL,$latitude = NULL){
        $this->longitude = $longitude;
        $this->latitude = $latitude;
    }
    
}