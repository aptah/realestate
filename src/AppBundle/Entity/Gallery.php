<?php
namespace AppBundle\Entity;

use AppBundle\Entity\Traits\ActiveableTrait;
use AppBundle\Entity\Traits\BlameableTrait;
use AppBundle\Entity\Traits\NameableTrait;
use AppBundle\Entity\Traits\TimestampableTrait;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\OrderBy as OrderBy;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Validator\Constraints as Assert;
use \Doctrine\Common\Collections\ArrayCollection;

/**
 * Gallery
 *
 * @ORM\Table(name="gallery")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\GalleryRepository")
 */
class Gallery
{
    use NameableTrait;
//    use BlameableTrait;
    use TimestampableTrait;
    use ActiveableTrait;

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     * @Gedmo\Slug(fields={"name"})
     * @ORM\Column(name="slug", type="string", length=255, nullable=false)
     */
    private $slug;

    /**
     * @var string
     *
     * @ORM\Column(name="cover_image", type="string", length=255, nullable=true)
     */
    private $coverImage;

    /**
     * @var GalleryImage
     *
     * @ORM\OneToMany(targetEntity="GalleryImage", mappedBy="gallery", cascade={"persist", "remove"})
     * @OrderBy({"position" = "ASC"})
     */
    private $galleryImages;

//    /**
//     *
//     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Article", mappedBy="gallery")
//     */
//    private $articles;

    /**
     *
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\RealEstates", mappedBy="gallery")
     */
    private $realEstates;
    
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->galleryImages = new ArrayCollection();
        $this->articles = new ArrayCollection();
    }


    public function __toString()
    {
        return $this->name;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set slug
     *
     * @param  string $slug
     * @return Gallery
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;

        return $this;
    }

    /**
     * Get slug
     *
     * @return string
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * Set coverImage
     *
     * @param  string $coverImage
     * @return Gallery
     */
    public function setCoverImage($coverImage)
    {
        $this->coverImage = $coverImage;

        return $this;
    }

    /**
     * Get coverImage
     *
     * @return string
     */
    public function getCoverImage()
    {
        return $this->coverImage;
    }

    /**
     * Add galleryImages
     *
     * @param  GalleryImage $galleryImage
     * @return Gallery
     */
    public function addGalleryImage(GalleryImage $galleryImage)
    {
        $this->galleryImages[] = $galleryImage;
        $galleryImage->setGallery($this);

        return $this;
    }


    /**
     * Remove galleryImage
     *
     * @param  GalleryImage $galleryImage
     * @return Gallery
     */
    public function removeGalleryImage(GalleryImage $galleryImage)
    {
        $galleryImage->setGallery();
        $this->galleryImages->removeElement($galleryImage);

        return $this;
    }

    /**
     * Get galleryImages
     *
     * @return ArrayCollection
     */
    public function getGalleryImages()
    {

        return $this->galleryImages;
    }

//    /**
//     * Add article
//     *
//     * @param  Article $article
//     * @return Gallery
//     */
//    public function addArticle(Article $article)
//    {
//        $this->articles[] = $article;
//
//        return $this;
//    }

//    /**
//     * Remove article
//     *
//     * @param  Article $article
//     * @return Gallery
//     */
//    public function removeArticle(Article $article)
//    {
//        $this->articles->removeElement($article);
//    }
//
//    /**
//     * Get article
//     *
//     * @return ArrayCollection
//     */
//    public function getArticles()
//    {
//        return $this->articles;
//    }
}
