<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Embedded\GeoPoint;
use AppBundle\Entity\Gallery;
use AppBundle\Entity\RealEstates;
use AppBundle\Entity\RealEstatesTranslation;
use AppBundle\Repository\RealEstatesRepository;
use Elastica\Filter\GeoBoundingBox;
use Elastica\Query\BoolQuery;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\View\View;
use Symfony\Component\Form\FormError;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use FOS\RestBundle\Controller\Annotations\Put;
use FOS\RestBundle\Controller\Annotations\Post;
use FOS\RestBundle\Controller\Annotations\RequestParam;
use Symfony\Component\Validator\ConstraintViolation;


class ApiController extends FOSRestController
{

    /**
     * Make real estate inactive
     *
     * @param RealEstates $realEstates
     * @return JsonResponse
     */
    public function realEstateDeactivationAction(RealEstates $realEstates)
    {

        $em = $this->getDoctrine()->getManager();

        $realEstates->setActive(false);
        $em->persist($realEstates);
        $em->flush();

        return View::create()
            ->setStatusCode(200)
            ->setData([
                'success' => true,
            ]);
    }


    /**
     * Update `viewed` field for real estate
     *
     * @param RealEstates $realEstates
     * @param Request $request
     * @return JsonResponse
     */
    public function realEstateChangeViewAction(RealEstates $realEstates, Request $request)
    {
        $value = $request->get('value');

        if (!$value) {
            return View::create()
                ->setStatusCode(500)
                ->setData([
                    'success' => false,
                    'message' => 'Value is required',
                ]);
        }

        if ($value < 0) {

            return View::create()
                ->setStatusCode(500)
                ->setData([
                    'success' => false,
                    'message' => 'Value can\'t be less than 0',
                ]);
        }

        $em = $this->getDoctrine()->getManager();

        $realEstates->setViewed($value);
        $em->persist($realEstates);
        $em->flush();

        return View::create()
            ->setStatusCode(200)
            ->setData([
                'success' => true,
                'viewed' => $value,
            ]);
    }

    /**
     * Create new real estate
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function addRealEstateAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $validator = $this->get('validator');

        $geoPoint = new GeoPoint($request->get('longitude'), $request->get('latitude'));

        $errors = $validator->validate($geoPoint);

        if (count($errors) > 0) {
            return $this->validationErrorHandler('geo_point', $errors);
        }
        
        $realEstates = new RealEstates();

        $realEstates->setSize($request->get('size'));
        $realEstates->setPrice($request->get('price'));
        $realEstates->setIsForSale($request->get('isForSale'));
        $realEstates->setViewed($request->get('viewed'));
        $realEstates->setActive($request->get('active'));
        $realEstates->setInsertDate(new \DateTime());
        $realEstates->setUpdateDate(new \DateTime());
        $realEstates->setGeoPoint($geoPoint);
        

        if ($request->get('gallery')) {
            $gallery = new Gallery();

            $gallery->setSlug($request->get('gallery[slug]', null, true));
            $gallery->setCoverImage($request->get('gallery[coverImage]', null, true));
            $gallery->setName($request->get('gallery[name]', null, true));
            $gallery->setInsertDate(new \DateTime());
            $gallery->setUpdateDate(new \DateTime());
            $gallery->setActive($request->get('gallery[active]', null, true));

            $errors = $validator->validate($gallery);

            if (count($errors) > 0) {
                return $this->validationErrorHandler('gallery', $errors);
            }

            $em->persist($gallery);

            $realEstates->setGallery($gallery);
        }

        if ($request->get('translations')) {
            foreach ($request->get('translations') as $translationData) {
                $translation = new RealEstatesTranslation();
                $translation->setSlug(!empty($translationData['slug']) ? $translationData['slug'] : null);
                $translation->setContent(!empty($translationData['content']) ? $translationData['content'] : null);
                $translation->setLocale(!empty($translationData['locale']) ? $translationData['locale'] : null);
                $translation->setTitle(!empty($translationData['title']) ? $translationData['title'] : null);

                $errors = $validator->validate($translation);

                if (count($errors) > 0) {
                    return $this->validationErrorHandler('translation', $errors);
                }

                $em->persist($translation);
                $realEstates->addTranslation($translation);
            }
        }

        $em->persist($realEstates);
        $em->flush();

        return View::create()
            ->setFormat('json')
            ->setStatusCode(200)
            ->setData($realEstates);
    }

    /**
     * Search real estates by some fields (size, price, longitude, latitude)
     *
     * @param Request $request
     * @return View
     */
    public function searchAction(Request $request)
    {
        $values = [];
        $arrayStrings = ['size', 'price', 'longitude', 'latitude'];

        $values['size'] = $request->get('size');
        $values['price'] = $request->get('price');
        $values['longitude'] = $request->get('longitude');
        $values['latitude'] = $request->get('latitude');

        if (!$values['size'] && !$values['price'] && !$values['longitude'] && !$values['latitude']) {
            return View::create()
                ->setStatusCode(401)
                ->setData([
                    'success' => false,
                    'message' => 'At least one parameter (size, price, longitude, latitude should be sent',
                ]);
        }

        foreach ($arrayStrings as $string) {
            if ($values[$string]) {
                if (empty($values[$string]['min']) || empty($values[$string]['max']) || !is_numeric($values[$string]['min']) || !is_numeric($values[$string]['max'])) {
                    return View::create()
                        ->setStatusCode(401)
                        ->setData([
                            'string' => $string,
                            'success' => false,
                            'message' => $string . ' should have min and max options and option should be numeric',
                        ]);
                }
            }
        }

        $em = $this->getDoctrine()->getManager();

        /** @var RealEstatesRepository $realEstatesRepository */
        $realEstatesRepository = $em->getRepository('AppBundle:RealEstates');

        return View::create()
            ->setStatusCode(200)
            ->setData([
                'success' => true,
                'data' => $realEstatesRepository->search($values, $arrayStrings),
            ]);
    }

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function searchOnMapAction(Request $request)
    {
        $coordinates = $request->get('coordinates');

        if (!$coordinates) {
            return View::create()
                ->setStatusCode(401)
                ->setData([
                    'success' => false,
                    'message' => 'Coordinates are not received',
                ]);
        }

        $data = [];

        foreach ($coordinates as $coordinate) {
            $data[] = $coordinate['longitude'] . ', ' . $coordinate['latitude'];
        }

//        $finder = $this->container->get('fos_elastica.finder.app.realEstates');

        $boolQuery = new BoolQuery();
        $geoQuery = new GeoBoundingBox('geoPoint', $data);

//        $boolQuery->addMust($geoQuery);
//        $results = $finder->find($boolQuery);
//        foreach($results as $result) {
//            echo $result->getId()."; ".$result->getGeoPoint()." ".$result->getTitle()."<br />";
//        }

        return View::create()
            ->setStatusCode(200)
            ->setData([
                'success' => true,
                'data' => 'Provide here object(s)'
            ]);
    }


    /**
     * Handle entity validation
     *
     * @param string $key
     * @param ConstraintViolation[] $errors
     * @return View
     */
    public function validationErrorHandler($key, $errors)
    {
        $data[$key] = [];

        foreach ($errors as $error) {
            $data[$key][$error->getPropertyPath()] = $error->getMessage();
        }

        return View::create()
            ->setStatusCode(400)
            ->setData($data);
    }
}