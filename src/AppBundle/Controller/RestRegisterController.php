<?php
/**
 * Created by PhpStorm.
 * User: vladymyr
 * Date: 05.04.17
 * Time: 23:43
 */

namespace AppBundle\Controller;

use AppBundle\Entity\User;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\View\View;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class RestRegisterController extends FOSRestController
{
    public function registerAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $validator = $this->get('validator');

        $email = $request->get('email');
        $username = $request->get('username');
        $password = $request->get('password');
        
        $user = new User();

        $user->setEmail($email);
        $user->setUsername($username);
        $user->setPlainPassword($password);
        $user->setEnabled(true);

        $errors = $validator->validate($user);

        if (count($errors) > 0) {
            $data['user'] = [];

            foreach ($errors as $error) {
                $data['user'][$error->getPropertyPath()] = $error->getMessage();
            }

            return View::create()
                ->setStatusCode(400)
                ->setData($data);
        }

        $em->persist($user);
        $em->flush();

        return View::create()
            ->setStatusCode(200)
            ->setData([
                'success' => true,
                'data' => $user,
            ]);
    }

}