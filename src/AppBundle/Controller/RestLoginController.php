<?php

namespace AppBundle\Controller;

use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Routing\ClassResourceInterface;
use FOS\RestBundle\Controller\Annotations\RouteResource;
use FOS\RestBundle\View\View;
use Lexik\Bundle\JWTAuthenticationBundle\Exception\JWTEncodeFailureException;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;


class RestLoginController extends FOSRestController implements ClassResourceInterface
{
    /**
     * @param Request $request
     * @return JsonResponse
     * @throws JWTEncodeFailureException
     */
    public function tokenAuthenticationAction(Request $request)
    {
        $email = $request->request->get('email');
        $password = $request->request->get('password');

        if (!$email) {
            return View::create()
                ->setStatusCode(401)
                ->setData([
                    'email' => 'Email is required',
                ]);
        }

        if (!$password) {
            return View::create()
                ->setStatusCode(401)
                ->setData([
                    'password' => 'Password is required',
                ]);
        }

        $user = $this->getDoctrine()->getRepository('AppBundle:User')
            ->findOneBy(['email' => $email]);

        if(!$user) {
            throw $this->createNotFoundException();
        }

        // password check
        if(!$this->get('security.password_encoder')->isPasswordValid($user, $password)) {
            throw $this->createAccessDeniedException();
        }

        // Use LexikJWTAuthenticationBundle to create JWT token that hold only information about user name
        $token = $this->get('lexik_jwt_authentication.encoder')
            ->encode(['username' => $user->getUsername()]);

        // Return generated token
        return new JsonResponse(['token' => $token]);
    }
}
