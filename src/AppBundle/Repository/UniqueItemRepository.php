<?php

namespace AppBundle\Repository;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\NoResultException;

class UniqueItemRepository extends EntityRepository
{

    const CACHE_KEY_FORMAT = 'unique_item_%s';

    public function getUniqueContentBySlug($slug)
    {
        try {
            return $this->createQueryBuilder('unique')
                ->select('unique.content')
                ->where('unique.slug = :slug')
                ->setParameter('slug', $slug)
                ->getQuery()
                ->useResultCache(true)
                ->setResultCacheId(sprintf(self::CACHE_KEY_FORMAT, $slug))
                ->getSingleScalarResult();
        } catch (NoResultException $e) {
            return "";
        }
    }
}
