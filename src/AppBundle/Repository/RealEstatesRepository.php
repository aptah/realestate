<?php

namespace AppBundle\Repository;

use Doctrine\ORM\EntityRepository;

class RealEstatesRepository extends EntityRepository
{
    public function search($values, $strings)
    {
        $whereIsUsed = false;
        $query = $this->createQueryBuilder('r');
        
        foreach ($strings as $string) {
            if ($values[$string]) {
                if (!$whereIsUsed) {
                    $query->where('r.' . $string . ' BETWEEN :min' . $string . ' and :max' . $string);
                    $query->setParameter(':min' . $string, $values[$string]['min']);
                    $query->setParameter(':max' . $string, $values[$string]['max']);
                    $whereIsUsed = true;
                } else {
                    $query->andWhere('r.' . $string . ' BETWEEN :min' . $string . ' and :max' . $string);
                    $query->setParameter(':min' . $string, $values[$string]['min']);
                    $query->setParameter(':max' . $string, $values[$string]['max']);
                }

            }
        }

        return $query
            ->getQuery()
            ->getResult();
    }
}
