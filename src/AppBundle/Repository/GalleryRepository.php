<?php

namespace AppBundle\Repository;

use Doctrine\ORM\EntityRepository;

use Gedmo\Mapping\Annotation as Gedmo;

class GalleryRepository extends \Gedmo\Sortable\Entity\Repository\SortableRepository
{

    public function getGalleryWithImages($gallery)
    {
            $builder = $this->createQueryBuilder('gallery')
                ->select('gallery, galleryImages')
                ->leftJoin('gallery.galleryImages', 'galleryImages');

        if (is_string($gallery)) { // ha string a gallery változó akkor slug-ra keresünk
            $builder = $builder
                ->andWhere('gallery.slug = :slug')
                ->setParameter('slug', $gallery);
        } elseif (is_numeric($gallery)) {// ha szám a gallery változó akkor ID-re keresünk
            $builder = $builder
                ->andWhere('gallery.id = :id')
                ->setParameter('id', $gallery);
        } elseif (is_object($gallery)) {// ha objektum a gallery változó akkor objektumra keresünk
            $builder = $builder
                ->andWhere('gallery = :gallery')
                ->setParameter('gallery', $gallery);
        }

            return $builder->getQuery()->getOneOrNullResult();
    }
}
